# Bloodstain Pattern Analysis (BPA) Book Images

The images in this repository are for my article on [HemoSpat BPA Software](https://hemospat.com) in the _Digital Technology for BPA and Crime Scene Analysis_ chapter of an upcoming textbook on bloodstain pattern analysis.


<p xmlns:cc="https://creativecommons.org/ns#" xmlns:dct="https://purl.org/dc/terms/">
    These images © 2021 by <span property="cc:attributionName">Andy Maloney</span> are licensed under
    <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0
        <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg">
        <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg">
    </a>
</p>